/*
Desarrollado por: Luis Fernando Diaz Devos
Fecha: 28/05/2018
*/

//Variables de trabajo
//Importamos
var express = require('express');
var bodyParser = require('body-parser');
var requestJson = require('request-json');
var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var app = express();
//variables de entorno de trabajo
var URLbase = "/colapi/v3/";
var port = process.env.PORT || 3000;
//Ruta de base de dato remota
var baseMLabUrl = 'https://api.mlab.com/api/1/databases/colapidb_lfdd/collections/';
var apiKeyMLab = 'apiKey=5-_vDTuGpW4xbUeAbO_0v6bUOA39U2tI';

//Indicador de instancia de inicio
app.listen(port, function(){
	console.log("API-V3 Trabajando en el puerto: " + port + "...");
	/*
	var token = jwt.sign({ foo: 'bar' }, 'shhhhh');
	console.log(token);
	jwt.verify(token, 'shhhhh', function(err, decoded) {
  		console.log("entre") // bar
  		console.log(decoded.foo) // bar
  		console.log(decoded.err) // bar
	});*/
});
//Funcion para poder atender los Json del body
app.use(bodyParser.json());
//
//------------- Usuarios
//
app.get(URLbase + 'user',
 	function(req, res) {
		console.log(" -GET: - Ini - User...");
		var httpClient = requestJson.createClient(baseMLabUrl);
		console.log(" |	-> Se consulta lista de clientes.");
		var queryString = 'user?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de movement en mlab"}
				console.log(" -GET: - Fin - User...");
				res.send(respuesta);
		});
});//Funcion encargada de consultar la lista de clientes
app.get(URLbase + 'user/:id',
 	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de la informacion del cliente en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta de un cliente
app.get(URLbase + 'user/:id/account', 	
	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'account?f={"_id": 0}&q={"accountUserId":'+id+'}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de la cuentas en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta d las cuentas de un cliente
app.get(URLbase + 'user/:id/account/:iban',
	function(req, res) {
		var id=req.params.id;
		var iban=req.params.iban;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'account?f={"_id": 0}&q={"accountUserId":'+id+',"accountIban":"'+iban+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de la cuentas en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta de una cuenta de un cliente 
app.get(URLbase + 'user/:id/account/:iban/movement',
	function(req, res) {
		var iban=req.params.iban;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'movement?f={"_id": 0}&q={"movementIban":"'+iban+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de la cuentas en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta los movimiento de una cuenta para un cliente
app.post(URLbase + 'user',
 	function(req, res) {
 		console.log(req.body);
 		var avatar = req.body.avatar;
 		var mensaje = {"msg":"Error en MLab"}
 		if (req.body.userName ){
 			if (req.body.userApellido){
 				if (req.body.userEmail){
 					if (req.body.userPassword){
	 					if (req.body.cuentas){
	 						if (req.body.cuentas.length>0){
	 							if (!req.body.avatar){
	 								avatar = '';
	 							}
								mensaje = 'ok';
	 						}else{mensaje = {"msg":"Se debe indicar minimo 1 cuenta"}}
	 					}else{mensaje = {"msg":"Cuentas no informadas"}}
 					}else{mensaje = {"msg":"Password no informado"}}
 				}else{mensaje = {"msg":"Email no informado"}}
 			}else{mensaje = {"msg":"Apellido no informado"}}
 		}else{mensaje = {"msg":"Usuario no informado"}}
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'user?f={"userEmail": 1,"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de la informacion del cliente en mlab"}
				for (var i = 0; i < respuesta.length; i++) {
				    if (respuesta[i].userEmail == req.body.userEmail){
				    	mensaje = {"msg":"Usuario ya registrado"}
				    }
				}
				if (mensaje == 'ok'){
					var httpClient2 = requestJson.createClient(baseMLabUrl + 'user?' + apiKeyMLab);
					var queryString = 'user?' + apiKeyMLab;
					var newUser = {
							  "userId" : respuesta.length + 1,
						    "userName" : req.body.userName,
						"userApellido" : req.body.userApellido,
						   "userEmail" : req.body.userEmail,
						"userPassword" : req.body.userPassword,
						  "userStatus" : "Active",
						      "avatar" : avatar,
						     "cuentas" : req.body.cuentas
					};
					httpClient2.post('',newUser,
						function(err,respuestaMLab,body){
							mensaje = body;
			 				res.send(mensaje);
					});
				}else{
					res.send(mensaje);
				}
		});
});//Funcion encargada de crear un usuario
app.put(URLbase + 'user/:id',
	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'user?q={"userId":'+id+'}&' + apiKeyMLab;
		var respuesta = "";
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de la informacion del cliente en mlab"};
				if (!err){
			 		if (req.body.userName){	
				 		body[0].userName = req.body.userName ;
				 	}
			 		if (req.body.userApellido){
				 		body[0].userApellido = req.body.userApellido;
				 	}
			 		if (req.body.userEmail){
				 		body[0].userEmail = req.body.userEmail;
				 	}
			 		if (req.body.userPassword){
				 		body[0].userPassword = req.body.userPassword;
				 	}
				 	if (req.body.avatar){
				 		body[0].avatar = req.body.avatar;
				 	}
				 	if (req.body.cuentas.length>0){
				 		//Se actualiza las cuentas del cliente
				 		body[0].cuentas = req.body.cuentas;
				 	}
				 	var httpClient2 = requestJson.createClient(baseMLabUrl + 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab);
					httpClient2.post('',body[0],
						function(err,respuestaMLab2,body2){
							respuesta = !err ? body : {"msg":"Error en la realizar la modificacion"};
							console.log(respuesta);
			 				res.send(respuesta);
					});
				}else{
					respuesta = {"msg":"El usuario no existe"};
					res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la modificacion de un cliente
app.delete(URLbase + 'user/:id',
	function(req, res) {
		var id=req.params.id;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab;
		var respuesta = ""
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					body[0].userStatus = "Disable";
				 	var httpClient2 = requestJson.createClient(baseMLabUrl + 'user?f={"_id": 0}&q={"userId":'+id+'}&' + apiKeyMLab);
					httpClient2.put('',body[0],
						function(err,respuestaMLab2,body2){
							respuesta = !err ? body : {"msg":"Error en la realizar la modificacion"};
			 				res.send(respuesta);
					});
				}else{
					respuesta = {"msg":"Error el usuario no existe"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la eliminacion de un cliente de forma logica
//
//------------- Cuentas
//
app.get(URLbase + 'account',
 	function(req, res) {
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'account?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de movement en mlab"}
				res.send(respuesta);
		});
});//Funcion encargada de consultar la lista de cuentas
app.get(URLbase + 'account/:accountId',
 	function(req, res) {
		var accountId=req.params.accountId;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'account?f={"_id": 0}&q={"accountIban":"'+accountId+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				console.log(body);
				console.log(queryString);
				var respuesta = !err ? body : {"msg":"Error en la carga de la informacion de la cuenta en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta de una cuenta
app.delete(URLbase + 'account/:accountId',
	function(req, res) {
		var accountId=req.params.accountId;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'account?f={"_id": 0}&q={"accountIban":"'+accountId+'"}&' + apiKeyMLab;
		var respuesta = ""
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					body[0].accountStatus = "Disable";
				 	var httpClient2 = requestJson.createClient(baseMLabUrl + 'account?f={"_id": 0}&q={"accountIban":"'+accountId+'"}&' + apiKeyMLab);
					httpClient2.put('',body[0],
						function(err,respuestaMLab2,body2){
							respuesta = !err ? body : {"msg":"Error en la realizar la modificacion"};
			 				res.send(respuesta);
					});
				}else{
					respuesta = {"msg":"Error la cuenta no existe"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la eliminacion logica de una cuenta
//
//------------- Movimientos
//
app.get(URLbase + 'movement',
 	function(req, res) {
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'movement?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de movement en mlab"}
				res.send(respuesta);
		});
});//Funcion encargada de consultar la lista de movimientos
app.get(URLbase + 'movement/:movementId',
 	function(req, res) {
		var movementId=req.params.movementId;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'movement?f={"_id": 0}&q={"movementId":'+movementId+'}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				console.log(body);
				console.log(queryString);
				var respuesta = !err ? body : {"msg":"Error en la carga de la informacion de los movimientos en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta de una cuenta
app.delete(URLbase + 'movement/:movementId',
	function(req, res) {
		var movementId=req.params.movementId;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'movement?f={"_id": 0}&q={"movementId":'+movementId+'}&' + apiKeyMLab;
		var respuesta = ""
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					body[0].movementStatus = "Disable";
				 	var httpClient2 = requestJson.createClient(baseMLabUrl + queryString);
					httpClient2.put('',body[0],
						function(err,respuestaMLab2,body2){
							respuesta = !err ? body : {"msg":"Error la realizar la modificacion"};
			 				res.send(respuesta);
					});
				}else{
					respuesta = {"msg":"Error la cuenta no existe"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la eliminacion logica de una cuenta
//
//------------- Oficinas
//
app.get(URLbase + 'office',
 	function(req, res) {
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'office?f={"_id": 0}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				var respuesta = !err ? body : {"msg":"Error en la carga de las oficinas en mlab"}
				res.send(respuesta);
		});
});//Funcion encargada de consultar la lista de movimientos
app.get(URLbase + 'office/:officeId',
 	function(req, res) {
		var officeId=req.params.officeId;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'office?f={"_id": 0}&q={"officeId":'+officeId+'}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				console.log(body);
				console.log(queryString);
				var respuesta = !err ? body : {"msg":"Error en la carga de la informacion de las oficinas en mlab"}
				res.send({respuesta});
		});
});//Funcion encargada de ralizar la consulta de una cuenta
///----------------------------------------------------------------pendiente
app.delete(URLbase + 'office/:officeId',
	function(req, res) {
		var officeId=req.params.officeId;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'office?q={"officeId":'+officeId+'}&' + apiKeyMLab;
		var respuesta = "";
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (!err){
					var dato = body[0]._id;
					var queryString2 = 'office/' + dato.$oid + '?' +apiKeyMLab;
				 	var httpClient2 = requestJson.createClient(baseMLabUrl + queryString2);
					httpClient2.delete('',
						function(err,respuestaMLab2,body2){
							console.log(queryString2);
							respuesta = !err ? body2 : {"msg":"Error en la realizar la modificacion"};
			 				res.send(respuesta);
					});
				}else{
					respuesta = {"msg":"Error la cuenta no existe"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de ralizar la eliminacion logica de una cuenta
//
//------------- Session 
//
app.post(URLbase + 'session/login',
 	function(req, res) {
 		var respuesta ="";
		var userEmail = req.body.userEmail;
		var userPassword = req.body.userPassword;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'user?f={"_id": 0}&q={"userEmail":"'+userEmail+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (body.length >0){
					if (body[0].userPassword == userPassword){
						var httpClient = requestJson.createClient(baseMLabUrl);
						var queryString = 'session?q={"sessionCorreo":"'+userEmail+'"}&' + apiKeyMLab;
				 		var respuesta ="";
						httpClient.get(queryString,
							function(err,respuestaMLab,body2){
								if (body2.length >0){
									respuesta = {"msg":"Error de credenciales, COD:23"};
							 		res.send(respuesta);
								}else{
							 		var httpClient2 = requestJson.createClient(baseMLabUrl + 'session?' + apiKeyMLab);
									var queryString = 'session?' + apiKeyMLab;
									var token = jwt.sign(req.body, 'colapiLfdd');
									var sessionNew = {
									   "sessionCorreo" : userEmail,
										"sessionToken" : token
									};
									httpClient2.post('',sessionNew,
										function(err,respuestaMLab,body3){
											respuesta = !err ? sessionNew : {"msg":"Error en el registro de mlab"}
											res.send(respuesta);
									});
								}
						});			
					}else{
						respuesta = {"msg":"Error de credenciales"};
				 		res.send(respuesta);
					}
				}else{
					respuesta = {"msg":"Error de credenciales"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de dar acceso 
app.post(URLbase + 'session/status',
 	function(req, res) {
 		var respuesta ="";
		var userEmail = req.body.userEmail;
		var userPassword = req.body.userPassword;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'user?f={"_id": 0}&q={"userEmail":"'+userEmail+'"}&' + apiKeyMLab;
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (body.length >0){
					if (body[0].userPassword == userPassword){
						var httpClient = requestJson.createClient(baseMLabUrl);
						var queryString2 = 'session?f={"sessionToken": 1,"_id": 0}&q={"sessionCorreo":"'+userEmail+'"}&' + apiKeyMLab;
						httpClient.get(queryString2,
							function(err,respuestaMLab,body2){
								if (body2.length >0){
									res.send(body2[0]);
								}else{
									respuesta = {"msg":"Error de credenciales3"};
							 		res.send(respuesta);
								}
						});
					}else{
						respuesta = {"msg":"Error de credenciales2"};
				 		res.send(respuesta);
					}
				}else{
					console.log(req.body);
					respuesta = {"msg":"Error de credenciales1"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada validar el estado de la session
app.post(URLbase + 'session/logout',
 	function(req, res) {
		var sessionCorreo = req.body.sessionCorreo;
		var sessionToken = req.body.sessionToken;
		var httpClient = requestJson.createClient(baseMLabUrl);
		var queryString = 'session?q={"sessionCorreo":"'+sessionCorreo+'"}&' + apiKeyMLab;
 		var respuesta ="";
		httpClient.get(queryString,
			function(err,respuestaMLab,body){
				if (body.length >0){
					if (body[0].sessionToken == sessionToken){
						var dato = body[0]._id;
						var queryString2 = 'session/' + dato.$oid + '?' +apiKeyMLab;
				 		var httpClient2 = requestJson.createClient(baseMLabUrl + queryString2);
						httpClient2.delete('',
						function(err,respuestaMLab2,body2){
							console.log(queryString2);
							respuesta = !err ? {"msg":"Session Cerrada"} : {"msg":"Error en la realizar la modificacion"};
			 				res.send(respuesta);
						});
					}else{
						respuesta = {"msg":"Error de credenciales2"};
				 		res.send(respuesta);
					}
				}else{
					respuesta = {"msg":"Error de credenciales1"};
			 		res.send(respuesta);
				}
		});
});//Funcion encargada de cerrar una session
//Fin del codigo ™, ®LFDD